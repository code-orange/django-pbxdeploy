from django.contrib import admin

from django.conf import settings
from django.urls import include, path
from django.urls import re_path
from django.http import HttpResponseRedirect

urlpatterns = [
    # path('', lambda r: HttpResponseRedirect('dashboard/')),
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path(r"^watchman/", include("watchman.urls")),
    path("deploy/", include("django_pbxdeploy_main.django_pbxdeploy_main.urls")),
    path(
        "picsrv/",
        include("django_pbxdeploy_image_server.django_pbxdeploy_image_server.urls"),
    ),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
